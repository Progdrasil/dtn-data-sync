package com.gud.dtds

import android.content.pm.PackageManager
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.gud.dtds.aware.WifiAwareable
import com.gud.dtds.components.EnsurePermissions
import com.gud.dtds.components.ErrorMessage
import com.gud.dtds.ui.theme.DelayTolerantDataSyncTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DelayTolerantDataSyncTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(16.dp)
                    ) {
                        EnsurePermissions { HomeScreen() }
                    }
                }
            }
        }
    }
}

@Composable
fun HomeScreen() {
    val context = LocalContext.current
    if (context.packageManager.hasSystemFeature(PackageManager.FEATURE_WIFI_AWARE)) {
        WifiAwareable()
    } else {
        ErrorMessage(message = "Wifi aware is not supported on this device")
    }
}
