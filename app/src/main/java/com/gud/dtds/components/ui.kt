package com.gud.dtds.components

import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun Center(content: @Composable () -> Unit) {
    Column(
        Modifier
            .padding(20.dp)
            .fillMaxSize()
            .wrapContentSize(Alignment.Center)
    ) {
        Row(Modifier.wrapContentSize(Alignment.Center)) {
            content()
        }
    }
}
