package com.gud.dtds.components

import androidx.compose.ui.graphics.Color

data class SessionState(val ready: SessionReadiness, val message: String) {
    companion object {
        fun ready(message: String) = SessionState(SessionReadiness.Ready, message)
        fun loading(message: String) = SessionState(SessionReadiness.Loading, message)
        fun error(message: String) = SessionState(SessionReadiness.Error, message)
    }

    fun loading(message: String) =
        if (isNotError()) {
            SessionState.loading(message)
        } else {
            this
        }

    fun ready(message: String) = if (isNotError()) {
        SessionState.ready(message)
    } else {
        this
    }

    fun error(message: String) = if (isNotError()) {
        SessionState.error(message)
    } else {
        this
    }

    fun isNotError() = ready != SessionReadiness.Error
}

enum class SessionReadiness(val color: Color) {
    Ready(Color.Green),
    Loading(Color.Cyan),
    Error(Color.Red)
}