package com.gud.dtds.components

import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.unit.sp

@Composable
fun ErrorMessage(message: String) {
    Center {
        Text(
            text = message,
//                textAlign = TextAlign.Center,
            color = MaterialTheme.colors.error,
            fontSize = 30.sp
        )
    }
}
