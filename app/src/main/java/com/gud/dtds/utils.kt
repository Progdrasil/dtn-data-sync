package com.gud.dtds

import java.nio.ByteBuffer
import java.util.*

const val AWARE_MESSAGE_SHARE = "DTN-MESSAGE-SHARE"
var uniqueID = UUID.randomUUID()

fun UUID.toBytes(): ByteArray {
    val buf = ByteBuffer.wrap(ByteArray(16))
    buf.putLong(mostSignificantBits)
    buf.putLong(leastSignificantBits)
    return buf.array()
}

fun ByteArray.toUuid(): UUID {
    val buf = ByteBuffer.wrap(this)
    val high = buf.long
    val low = buf.long
    return UUID(high, low)
}

fun String.toMessage(): ByteArray = encodeToByteArray().toMessage()

fun ByteArray.toMessage(): ByteArray {
    val buf = ByteBuffer.wrap(ByteArray(size + 16))
    buf.putLong(uniqueID.mostSignificantBits)
    buf.putLong(uniqueID.leastSignificantBits)
    buf.put(this)
    return buf.array()
}

fun ByteArray.extractId(): Pair<UUID, ByteArray> {
    val buf = ByteBuffer.wrap(this)
    val high = buf.long
    val low = buf.long
    val array = ByteArray(buf.remaining())
    buf.get(array)
    return UUID(high, low) to array
}
