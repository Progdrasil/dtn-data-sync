package com.gud.dtds

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Path

const val ENDPOINT = "https://mighty-depths-56260.herokuapp.com/"

interface BundleEndpoint {
    @Headers("Content-Type: application/json")
    @POST("/bundle/{name}")
    fun sendBundle(@Path("name") name: String, @Body body: OnlineMessage): Call<Unit>
}

val contentType = "application/json".toMediaType()
@ExperimentalSerializationApi
fun client() = Retrofit.Builder()
    .baseUrl(ENDPOINT)
    .addConverterFactory(Json.asConverterFactory(contentType))
    .build()
    .create(BundleEndpoint::class.java)