package com.gud.dtds.aware

import android.content.Context
import android.net.wifi.aware.AttachCallback
import android.net.wifi.aware.WifiAwareManager
import android.net.wifi.aware.WifiAwareSession
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.*
import androidx.compose.ui.platform.LocalContext
import com.gud.dtds.components.Center
import com.gud.dtds.components.ErrorMessage
import com.gud.dtds.components.SystemBroadcastReceiver

@Composable
fun WifiAwareable() {
    val context = LocalContext.current
    val wifiAwareManager = context.getSystemService(Context.WIFI_AWARE_SERVICE) as WifiAwareManager?
    var isWifiAwareAvailable by remember {
        mutableStateOf(wifiAwareManager?.isAvailable ?: false)
    }
    var awareSession: WifiAwareSession? by remember {
        mutableStateOf(null)
    }
    var error: String by remember {
        mutableStateOf("")
    }
    SystemBroadcastReceiver(systemAction = WifiAwareManager.ACTION_WIFI_AWARE_STATE_CHANGED) {
        isWifiAwareAvailable = wifiAwareManager?.isAvailable ?: false
    }
    when {
        error.isNotBlank() -> {
            awareSession?.close()
            ErrorMessage(message = error)
        }
        isWifiAwareAvailable -> {
            if (awareSession == null) {
                wifiAwareManager?.attach(object : AttachCallback() {
                    override fun onAttachFailed() {
                        super.onAttachFailed()
                        error = "Unable to attach aware session"
                    }

                    override fun onAttached(session: WifiAwareSession?) {
                        super.onAttached(session)
                        awareSession = session
                    }
                }, null)
            }

            awareSession?.let { AwareSession(session = it) }
                ?: Center {
                    CircularProgressIndicator()
                }
        }
        else -> {
            awareSession?.close()
            ErrorMessage(message = "Please enable location and wifi for this app to work")
        }
    }
}
