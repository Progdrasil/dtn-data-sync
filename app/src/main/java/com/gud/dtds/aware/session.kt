package com.gud.dtds.aware

import android.content.Context
import android.net.ConnectivityManager
import android.net.wifi.aware.*
import android.se.omapi.Session
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.selection.selectable
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Send
import androidx.compose.material.icons.filled.Wifi
import androidx.compose.material.icons.filled.WifiOff
import androidx.compose.runtime.*
import androidx.compose.runtime.snapshots.SnapshotStateMap
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.gud.dtds.*
import com.gud.dtds.components.*
import com.gud.dtds.ui.theme.DelayTolerantDataSyncTheme
import kotlinx.coroutines.launch
import kotlinx.serialization.ExperimentalSerializationApi
import java.util.*

class Peer(var handle: PeerHandle) {
    val messagesReceived = mutableStateListOf<String>()
}

fun SnapshotStateMap<UUID, Peer>.add(id: UUID, handle: PeerHandle) {
    when (val peer = get(id)) {
        null -> {
            put(id, Peer(handle))
        }
        else -> {
            peer.handle = handle
        }
    }
}

@ExperimentalSerializationApi
@Composable
fun AwareSession(session: WifiAwareSession) {
    var state by remember {
        mutableStateOf(SessionState.loading("Setup ready, building services"))
    }
    var pubSession: PublishDiscoverySession? by remember {
        mutableStateOf(null)
    }
    var subSession: SubscribeDiscoverySession? by remember {
        mutableStateOf(null)
    }
    val others = remember {
        mutableStateMapOf<UUID, Peer>()
    }
    val sessionName = remember {
        mutableStateOf("demo")
    }
    val isOnline = isOnline()

    if (pubSession == null) {
        val pubConfig = PublishConfig.Builder()
            .setServiceName(AWARE_MESSAGE_SHARE)
            .setPublishType(PublishConfig.PUBLISH_TYPE_UNSOLICITED)
            .setServiceSpecificInfo(uniqueID.toBytes())
            .build()
        session.publish(pubConfig, object : DiscoverySessionCallback() {
            override fun onPublishStarted(session: PublishDiscoverySession) {
                super.onPublishStarted(session)
                if (subSession == null) {
                    state = state.loading("Publish setup, waiting for subscribe")
                } else {
                    state = state.ready("Discovering other devices")
                }
                pubSession = session
            }

            override fun onMessageReceived(peerHandle: PeerHandle, message: ByteArray) {
                super.onMessageReceived(peerHandle, message)
                val (id, msg) = message.extractId()
                others.add(id, peerHandle)
                others[id]?.messagesReceived?.add(String(msg))
                if (isOnline) {
                    client().sendBundle(
                        sessionName.value,
                        OnlineMessage(id.toString(), String(msg))
                    )
                }
            }

            override fun onSessionConfigFailed() {
                super.onSessionConfigFailed()
                state = state.error("Could not publish DTN service")
            }
        }, null)
    }

    if (subSession == null) {
        val subConfig = SubscribeConfig.Builder()
            .setServiceName(AWARE_MESSAGE_SHARE)
            .setSubscribeType(SubscribeConfig.SUBSCRIBE_TYPE_PASSIVE)
            .setServiceSpecificInfo(uniqueID.toBytes())
            .build()
        session.subscribe(subConfig, object : DiscoverySessionCallback() {
            override fun onSubscribeStarted(session: SubscribeDiscoverySession) {
                super.onSubscribeStarted(session)
                if (pubSession == null) {
                    state.loading("Subscribe ready, waiting on publish")
                } else {
                    state = state.ready("Discovering other devices")
                }
                subSession = session
            }

            override fun onServiceDiscovered(
                peerHandle: PeerHandle,
                serviceSpecificInfo: ByteArray,
                matchFilter: MutableList<ByteArray>
            ) {
                super.onServiceDiscovered(peerHandle, serviceSpecificInfo, matchFilter)
                state = state.ready("Discovered peer ${serviceSpecificInfo.toUuid()}")
                others.add(serviceSpecificInfo.toUuid(), peerHandle)
            }

            override fun onSessionConfigFailed() {
                super.onSessionConfigFailed()
                state = state.error("Could not subscribe to DTN service")
            }
        }, null)
    }

    if (state.ready == SessionReadiness.Ready) {
        SessionView(state = state, peers = others, subSession!!, sessionName)
    } else {
        Column(Modifier.fillMaxSize()) {
            LoadingView(state = state)
        }
    }
}

@Composable
fun LoadingView(state: SessionState) {
    Row(
        Modifier
            .fillMaxWidth()
    ) {
        Text(text = "Acting as $uniqueID", color = MaterialTheme.colors.secondary)
    }
    Row {
        if (state.ready == SessionReadiness.Loading) {
            CircularProgressIndicator(color = state.ready.color)
        }
        Text(text = state.message, color = state.ready.color)
    }
}


@Composable
fun SessionView(
    state: SessionState,
    peers: Map<UUID, Peer>,
    session: SubscribeDiscoverySession,
    endpoint: MutableState<String>
) {
    var message by remember {
        mutableStateOf("")
    }
    var selection: UUID? by remember {
        mutableStateOf(null)
    }
    Column(Modifier.fillMaxSize()) {
        LoadingView(state = state)
        Row(Modifier.fillMaxWidth()) {
            TextField(
                value = endpoint.value,
                onValueChange = { endpoint.value = it },
                enabled = isOnline(),
                label = {
                    Text("Session Name")
                })
            val (icon, desc) = if (isOnline()) {
                Icons.Filled.Wifi to "online"
            } else {
                Icons.Filled.WifiOff to "offline"
            }
            Box(
                Modifier
                    .padding(16.dp), contentAlignment = Alignment.Center
            ) {
                Icon(imageVector = icon, contentDescription = desc)
            }
        }
        Row(
            Modifier
                .weight(1f)
                .border(1.dp, color = MaterialTheme.colors.onSurface)
                .padding(10.dp)
                .fillMaxWidth()
        ) {
            LazyColumn {
                items(items = peers[selection]?.messagesReceived ?: emptyList()) {
                    Row(
                        Modifier
                            .border(1.dp, color = MaterialTheme.colors.onSurface)
                    ) {
                        Box(Modifier.padding(5.dp)) {
                            Text(text = it, color = MaterialTheme.colors.onBackground)
                        }
                    }
                }
            }
        }
        Row(Modifier.fillMaxWidth()) {
            Column() {
                for (key in peers.keys) {
                    val colors = if (key == selection) {
                        MaterialTheme.colors.secondary to MaterialTheme.colors.onSecondary
                    } else {
                        MaterialTheme.colors.primary to MaterialTheme.colors.onPrimary
                    }
                    Row(
                        Modifier
                            .fillMaxWidth()
                            .background(color = colors.first)
                            .selectable(selected = key == selection) {
                                selection = if (key == selection) {
                                    null
                                } else {
                                    key
                                }
                            }) {
                        Box(modifier = Modifier.padding(5.dp)) {
                            Text(key.toString(), color = colors.second)
                        }
                    }
                }
            }
        }
        Row(
            Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            TextField(
                value = message,
                onValueChange = { message = it },
                modifier = Modifier.weight(1f),
            )
            Button(onClick = {
                session.sendMessage(peers[selection!!]!!.handle, 0, message.toMessage())
                message = ""
            }, enabled = selection != null && state.isNotError()) {
                Icon(imageVector = Icons.Filled.Send, contentDescription = "Send")
            }
        }
    }
}

@Composable
fun isOnline(): Boolean {
    val cm =
        LocalContext.current.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetwork = cm.activeNetwork
    return activeNetwork != null
}