package com.gud.dtds

import kotlinx.serialization.Serializable

@Serializable
data class OnlineMessage(val origin: String, val message: String)